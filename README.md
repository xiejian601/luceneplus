                            

### LucenePlus  让搜索开发更简单，更快捷，更容易维护 <br/>


### 简介
    lucenePlus 是基于 lucene 6.5 实现的，具有 易学易用、极其稳定、内置功能丰富 的全文搜索框架
入门视频：暂无
### 亮点
    。可视化动态词库自由添加
    。自定义Query 可以自由控制各种查询   又不必去  关心  检索问题 分页/高亮/排序/返回数据整理。。。  统统不用写
    。动态数据源  多个索引库  随意切换  不必  去写 一大坨 代码
    。性能方面  数据自动处理  会消耗一部分 性能  我们采用  配置文件来控制 对自动化数据的处理 性能提升 N 倍 几乎是原生 查询 并且配置相当简洁  以后一会一直简洁下去  ！！！
### 特点
    。多表分库设计
    。自定义非法过滤
    。自定义索引条件
    。自定义排序条件
    。自定义高亮显示
    。一键增删改查
    。内置中文分词器
    。自定义Query
    。各种查询支持
    。可视化动态词库

### 应用场景
    论坛、商城、音乐、电影、小说 等。。。。 各个领域
	
### 安装说明：
        建立IKAnalyzer.cfg.xml（用来控制词库）/（词库）ext.dic与（停用词库）stopword.dic文件
	把resources 文件下的 luceneHome.zip  解压到 d盘 根目录（可以是别的目录）运行：TestLucenePlus 测试类 里面的方法

### 文档：
        。config.xml  文件
        。里面的 field 是用来 定义字段	
        。每个 field 有type,name,isQuery,isSort,boost
        。type = int,float,binary,double,string,text  字母为小写
        。name = 字段名称
        。isQuery = 该字段是否参与查询  y 是  n 不
        。isSort = 升降排序  例： asc /  desc  <选填>
        。boost  = 权重  <选填>

### 集成:

        。[与项目集成](http://git.oschina.net/Myzhang/luceneplus/wikis/%E4%B8%8E%E9%A1%B9%E7%9B%AE%E9%9B%86%E6%88%90)

### 注意:
        。个人/公司 如果使用了LucenePlus 请在方便的情况下   在网站最底下  加上  本站搜索由LucenePlus 提供
        。有《本站搜索由LucenePlus 提供》标识的网站  免费获得作者的技术支持   请大家支持一下
        。框架支持JDK 版本 最低 1.8
### 界面预览
![输入图片说明](https://git.oschina.net/uploads/images/2017/0622/222525_ba6de3f0_440716.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0622/222534_506fa607_440716.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0622/222543_e802bdd3_440716.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0622/222601_2ccc1593_440716.png "在这里输入图片标题")






### 更新
        。2017-4-11 ： 增加权重、优化代码
        。2017-6-07 ： 增加范围、自定义query、修复以往bug、性能提升60%
        。2017-6-22 ： 可视化动态词库自由添加、修复异常处理、增加对外集成
### 性能测试 百万级
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/175937_8be64638_440716.png "在这里输入图片标题")