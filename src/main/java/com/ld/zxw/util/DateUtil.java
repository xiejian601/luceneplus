package com.ld.zxw.util;

import java.util.Date;

import org.apache.log4j.Logger;

public class DateUtil {
	
	private final static Logger log = Logger.getLogger(DateUtil.class);
	
	
	//获取当前时间
	public static long getDate(){
		return new Date().getTime();
	}
	
	public static void timeConsuming(String name,long time){
		long v = (getDate() - time);
		
		log.info(name+"--->执行耗时:"+BigDecimalUtil.divide(Double.valueOf(v), Double.valueOf(1000L))+"  秒");
		log.info(name+"--->执行耗时:"+v+"  毫秒");
	}
}
