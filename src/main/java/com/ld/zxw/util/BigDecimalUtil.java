package com.ld.zxw.util;

import java.math.BigDecimal;

public class BigDecimalUtil {
	
	public static double add(double v_1,double v_2){
		BigDecimal a = new BigDecimal(v_1);
		BigDecimal b = new BigDecimal(v_2);
		return a.add(b).doubleValue();
	}
	public static double divide(double v_1,double v_2){
		BigDecimal a = new BigDecimal(v_1);
		BigDecimal b = new BigDecimal(v_2);
		return a.divide(b).doubleValue();
	}
}
