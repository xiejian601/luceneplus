package com.ld.zxw.index;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;

import com.ld.zxw.config.Config;
import com.ld.zxw.config.IndexFactory;
import com.ld.zxw.util.CommonUtil;
import com.ld.zxw.util.DateUtil;
public class AddIndex implements IndexFactory{


	private Logger log = Logger.getLogger(AddIndex.class);
	
	@Override
	public boolean saveIndexs(Config config,List<Document> list) {
		long start_time = DateUtil.getDate();
		boolean flag = true;
		IndexWriter  indexWriter = CommonUtil.getIndexWriter(config);
		try {
			indexWriter.addDocuments(list);
			long commit = indexWriter.commit();
			log.info("提交返回:"+commit);
		} catch (IOException e) {
			flag = false;
			log.error("indexWriter.commit 失败", e);
		}finally {
			CommonUtil.colseIndexWriter(indexWriter);
		}
		DateUtil.timeConsuming("添加内核", start_time);
		return flag;
	}
}
