package com.ld.zxw.index;

import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;

import com.ld.zxw.config.Config;
import com.ld.zxw.config.IndexFactory;
import com.ld.zxw.util.CommonUtil;

public class DeleteIndex implements IndexFactory{

	private Logger log = Logger.getLogger(DeleteIndex.class);

	@Override
	public boolean deleteAll(Config config) {
		boolean flag =true;
		IndexWriter indexWriter = null;
		try {
			indexWriter = CommonUtil.getIndexWriter(config);
			indexWriter.deleteAll();
			indexWriter.commit();
		}catch (Exception e) {
			flag = false;
			log.error("清空索引-error:",e);
		}finally {
			CommonUtil.colseIndexWriter(indexWriter);
		}
		return flag;
	}
	@Override
	public boolean deletekey(Config config, String field, String value) {
		boolean flag =true;
		IndexWriter indexWriter = null;
		try {
			indexWriter = CommonUtil.getIndexWriter(config);
			indexWriter.deleteDocuments(new Term(field, value));
			indexWriter.commit();
		}catch (Exception e) {
			flag = false;
			log.error("清空索引-error:",e);
		}finally {
			CommonUtil.colseIndexWriter(indexWriter);
		}
		return flag;
	}
	@Override
	public boolean deleteQuery(Config config, Query query) {
		boolean flag =true;
		IndexWriter indexWriter = null;
		try {
			indexWriter = CommonUtil.getIndexWriter(config);
			indexWriter.deleteDocuments(query);
			indexWriter.commit();
		}catch (Exception e) {
			flag = false;
			log.error("清空索引-error:",e);
		}finally {
			CommonUtil.colseIndexWriter(indexWriter);
		}
		return flag;
	}
}
