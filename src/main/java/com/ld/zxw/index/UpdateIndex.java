package com.ld.zxw.index;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;

import com.ld.zxw.config.Config;
import com.ld.zxw.config.IndexFactory;
import com.ld.zxw.util.CommonUtil;
public class UpdateIndex implements IndexFactory{

	private Logger log = Logger.getLogger(UpdateIndex.class);
	
	@Override
	public boolean updateIndexs(Config config, List<Document> list,String field,String value) {
		boolean flag = true;
		IndexWriter  indexWriter = CommonUtil.getIndexWriter(config);
		try {
			indexWriter.updateDocuments(new Term(field,value), list);
			indexWriter.commit();
			indexWriter.flush();
		} catch (IOException e) {
			flag = false;
			log.error("indexWriter.commit  失败", e);
		}finally {
			CommonUtil.colseIndexWriter(indexWriter);
		}
		return flag;
	}
}
