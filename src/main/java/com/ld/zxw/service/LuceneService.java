package com.ld.zxw.service;

import java.util.HashMap;
import java.util.List;

import org.apache.lucene.search.Query;

import com.alibaba.fastjson.JSONArray;
import com.ld.zxw.page.Page;

/**
 * lucene 操作接口
 * @author Administrator
 *
 */
@SuppressWarnings("all")
public interface LuceneService {
	
	boolean saveObjs(List<Object> objs);
	
	boolean saveMaps(List<HashMap> objs);
	
	boolean delAll();
	
	boolean delKey(String field,String value);
	
	boolean deleteQuery(Query query);
	
	boolean updateObjs(List<Object> objs,String field,String value);
	
	<T> List<T>  findList(String value,Class<T> obj);
	
	<T> List<T> findRangeList(Query query,Class<T> obj);
	
	<T> Page<T> findPageList(String value,int pageNumber,int pageSize,Class<T> obj);
	
	<T> Page<T> findRangePageList(Query query,int pageNumber,int pageSize,Class<T> obj);
}
