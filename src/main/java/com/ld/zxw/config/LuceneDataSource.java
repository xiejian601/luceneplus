package com.ld.zxw.config;

import java.util.HashMap;
import java.util.Map;
/**
 * lucene 数据源
 * @author Administrator
 *
 */
public class LuceneDataSource {
	
	public static Map<String, Object> dataSource;
	
	//redis conf
	public static String redis_host = "127.0.0.1";
	public static int redis_prot = 6379;
	public static String redis_pwd = null;
	
	//默认不使用动态词典
	public static boolean DynamicDictionary = false;
	
	//默认数据源 key
	public final static String defaultKey = "defaultKey";
	
	public LuceneDataSource() {
		dataSource = new HashMap<>();
	}
	
	public void addDataSource(Config config,String key){
		dataSource.put(key, config);
	}
	
	public void addDataSource(Config config){
		dataSource.put(defaultKey, config);
	}
}
